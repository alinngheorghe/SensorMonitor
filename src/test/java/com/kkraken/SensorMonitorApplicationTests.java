package com.kkraken;

import com.kkraken.bean.filters.SensorFilter;
import com.kkraken.bean.response.SensorData;
import com.kkraken.bean.specification.SearchCriteria;
import com.kkraken.bean.specification.SensorSpecification;
import com.kkraken.repository.dao.Sensor;
import com.kkraken.repository.dao.SensorValue;
import com.kkraken.service.SensorService;
import com.kkraken.service.SensorValueService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SensorMonitorApplicationTests {

    public static final Long SENSOR_ID = 2L;
    @Autowired
    private SensorService sensorService;
    @Autowired
    private SensorValueService sensorValueService;

    @Test
    public void contextLoads() {
    }

    @Test
    public void addValues() {
      /*  int counter = 0;
        long range = 150;
        Sensor sensorData = sensorService.findById(SENSOR_ID);
        Random random = new Random();
        sensorValueService.clear();
        Calendar cal = Calendar.getInstance();

        Date oldDate = new Date();
        while (counter < 100) {
            sensorValueService.save(SensorValue.Builder.sensorValue().withValue((long)(random.nextDouble()*range)).withSensorId(SENSOR_ID).withUnit(sensorData.getUnit()).withTakenAt(oldDate).build());
            counter++;
            cal.setTime(oldDate);
            cal.add(Calendar.MINUTE, -1);
            oldDate = cal.getTime();
        }*/
    }

    @Test
    @Ignore
    public void clearValues(){
        /*sensorValueService.clear();*/
    }
}
