/**
 * Created by Kernel on 11/15/2016.
 */
(function () {
    angular.module('hs')
        .component('sidebar', {
            bindings: {},
            controller: SidebarController,
            templateUrl: "app/components/sidebar/sidebar.html?v=" + (new Date()).getTime()
        });
    SidebarController.$inject = [];
    function SidebarController() {
        var self = this;

        return self;
    }
})();
