/**
 * Created by Kernel on 11/23/2016.
 */
(function () {
    'use strict';

    angular.module('hs')
        .controller('SensorFormController', SensorFormController);

    SensorFormController.$inject = ['$mdDialog', 'locals', 'SensorDataFactory'];
    function SensorFormController($mdDialog, locals, SensorDataFactory) {
        var self = this;

        self.sensor = locals;

        self.action = {
            save: save,
            close: close
        };

        self.catalog = {
            types: [
                {key: "TEMP", description: "Temperature"},
                {key: "PRE", description: "Preasure"},
                {key: "HUM", description: "Humidity"}
            ],
            units: [
                {key: "C", description: "Celsius"},
                {key: "Pa", description: "Pascal"},
                {key: "Prc", description: "Percent"}
            ]
        };

        function save() {
            SensorDataFactory.save(self.sensor)
                .then(function (response) {
                    $mdDialog.hide(response.data);
                });
        }

        function close() {
            $mdDialog.cancel();
        }

        return self;
    }
})();
