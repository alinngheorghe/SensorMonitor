/**
 * Created by Alin N. Gheorghe on 17.12.2016.
 */
(function () {
    angular.module('hs')
        .component('weather', {
            bindings: {
                data : "="
            },
            controllerAs: 'vm',
            controller: WeatherController,
            templateUrl: "app/components/weather/weather.html?v=" + (new Date()).getTime()
        });
    WeatherController.$inject = [];
    function WeatherController() {
        var self = this;
        return self;
    }
})();
