/**
 * Created by Kernel on 11/15/2016.
 */
(function () {
    angular.module('hs')
        .component('headerBar', {
            bindings: {},
            controllerAs: 'vm',
            controller: HeaderController,
            templateUrl: "app/components/header/header.html?v=" + (new Date()).getTime()
        });
    HeaderController.$inject = ['$mdSidenav', 'OAuthService'];
    function HeaderController($mdSidenav, OAuthService) {
        var self = this;
        self.action = {toggle: toggle, logout: OAuthService.logout};
        function toggle() {
            $mdSidenav('left').toggle();
        }

        return self;
    }
})();
