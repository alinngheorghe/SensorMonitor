/**
 * Created by Kernel on 11/15/2016.
 */
(function () {
    angular.module('hs')
        .component('address', {
            bindings: {
                address : '='
            },
            controllerAs: 'vm',
            controller: AddressController,
            templateUrl: "app/components/address/address.html?v=" + (new Date()).getTime()
        });
    AddressController.$inject = [];
    function AddressController() {
        var self = this;
        return self;
    }
})();
