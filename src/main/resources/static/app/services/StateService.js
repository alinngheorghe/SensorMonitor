/**
 * Created by Kernel on 12/4/2016.
 */
angular.module('hs')
    .service('StateService', function ($state) {
        var self = this;

        self.to = {
            login : toLogin,
            home : toHome
        }

        function toLogin(){
            $state.go('security');
        }
        function toHome(){
            $state.go('main.home');
        }

        return self;
    });