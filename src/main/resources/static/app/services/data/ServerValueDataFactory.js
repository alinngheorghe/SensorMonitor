/**
 * Created by Kernel on 11/21/2016.
 */
(function(){
    angular.module('hs')
        .factory("ServerValueDataFactory", ServerValueDataFactory);

    ServerValueDataFactory.$inject = ['$http'];
    function ServerValueDataFactory($http){
        var baseUrl = '/frontend/sensor-value/';

        return {
            findById : findById,
            findByIds : findByIds,
            findByFilter : findByFilter,
            findByFilterUnpaged : findByFilterUnpaged
        };
        function findById(id){
            return $http.get( baseUrl + 'v1/get/' + id);
        }

        function findByFilter(filter){
            return $http.post(baseUrl + 'v1/getByFilter', filter);
        }


        function findByFilterUnpaged(filter){
            return $http.post(baseUrl + 'v1/findByFilterUnpaged', filter);
        }

        function findByIds(ids){
            return $http.post(baseUrl + 'v1/get/bySensorIds', ids);
        }


    }
})();
