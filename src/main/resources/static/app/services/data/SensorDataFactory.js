/**
 * Created by Kernel on 11/21/2016.
 */
(function(){
    angular.module('hs')
        .factory("SensorDataFactory", SensorDataFactory);

    SensorDataFactory.$inject = ['$http'];
    function SensorDataFactory($http){
        var baseUrl = '/frontend/sensor/';
        return {
            save : save,
            remove : remove,
            findById : findById,
            updateStatus : updateStatus,
            findByFilter : findByFilter,
            findByIdWithRecords : findByIdWithRecords
        };
        function findById(id){
            return $http.get( baseUrl + 'v1/get/' + id);
        }

        function findByIdWithRecords(id){
            return $http.get( baseUrl + 'v1/getWithRecords/' + id);
        }

function findByIds(ids){
    return $http.post( baseUrl + 'v1/getWithRecords/' + id);
}

        function save(sensor){
            return $http.post(baseUrl + 'v1/save', sensor);
        }

        function updateStatus(sensor){
            return $http.post(baseUrl + 'v1/updateStatus', sensor);
        }

        function remove(sensors){
            return $http.post(baseUrl + 'v1/delete', sensors);
        }
        function findByFilter(filter){
            return $http.post(baseUrl + 'v1/getByFilter', filter);
        }
    }
})();
