/**
 * Created by Alin N. Gheorghe on 03.01.2017.
 */

(function () {
    'use strict'
    angular.module('hs')
        .service("WeatherFactory", WeatherFactory);

    WeatherFactory.$inject = ['$q','$http'];
    function WeatherFactory($q, $http) {
        var self = this;

        self.get = get;

        var WEATHER_API_KEY = '9978a4b5ee3a81f4d1cce87815ee777a';
        var WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/forecast?lat={0}&lon={1}&units=metric&APPID={2}";
        function get(latitude, longitude) {
            var deferred = $q.defer();
            $http.get(WEATHER_API_URL.replace('{0}', latitude).replace('{1}', longitude).replace('{2}', WEATHER_API_KEY))
                .then(function (response) {
                    var data = response.data;
                    var groups = _.groupBy(data.list, function (entry) {
                        return moment(new Date(entry.dt_txt)).startOf('day').format("DD/MM/YYYY");
                    });
                    var days = _.map(groups, function (group, key) {
                        var length = (group.length || 1);
                        return new WeatherObject(key,
                            group.map(function (s) {
                                return s.main.humidity
                            }).reduce(function (sum, a) {
                                return sum + a;
                            }, 0) / length,
                            group.map(function (s) {
                                return s.main.pressure
                            }).reduce(function (sum, a) {
                                return sum + a;
                            }, 0) / length,
                            group.map(function (s) {
                                return s.main.temp
                            }).reduce(function (sum, a) {
                                return sum + a;
                            }, 0) / length,
                            group.map(function (s) {
                                return s.main.temp_min
                            }).reduce(function (sum, a) {
                                return sum + a;
                            }, 0) / length,
                            group.map(function (s) {
                                return s.main.temp_max
                            }).reduce(function (sum, a) {
                                return sum + a;
                            }, 0) / length,
                            group.map(function (s) {
                                return s.wind.speed
                            }).reduce(function (sum, a) {
                                return sum + a;
                            }, 0) / length
                        );
                    });
                    deferred.resolve( {
                        current: days[0],
                        days: days.slice(1, 5)
                    });
                });
            return deferred.promise;
        }
        return self;
    }
})();
