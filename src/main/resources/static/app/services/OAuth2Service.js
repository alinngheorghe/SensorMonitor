/**
 * Created by Kernel on 11/20/2016.
 */

angular.module('Security', ['ui.router'])
    .service('OAuthService', function ($q, $http, $cookies, $httpParamSerializer, StateService) {
        var self = this;
        self.token = {
            access: null,
            refresh: null
        };
        self.client = {
            id: null,
            secret: null
        };

        self.baseURL = null;
        self.grant_type = null;

        self.config = config;
        self.login = login;
        self.logout = logout;
        self.register = register;

        var TOKEN = 'token';

        function config(grant_type, baseUrl, client) {
            self.grant_type = grant_type;
            self.baseUrl = baseUrl;
            self.client = client;
        }

        function register(data) {
            debugger;
            $http.post('user/register', data)
                .then(function (response) {
                    login(data.username, data.password);
                })
                .catch(function (response) {
                });
        }

        function login(credentials) {
            $http.post("http://localhost:8080/auth", {username: credentials.username, password: credentials.password})
                .then(function(response){
                    $cookies.put(TOKEN, response.data.token);
                    StateService.to.home();
                });
        }

        function logout() {
            $cookies.remove(TOKEN);
            StateService.to.login();

        }

        return self;
    });