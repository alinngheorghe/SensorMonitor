/**
 * Created by Kernel on 11/15/2016.
 */
(function () {
    angular.module('hs')
        .controller('MainController', MainController);

    MainController.$inject = ['$mdSidenav', '$cookies', '$http', 'StateService'];
    function MainController($mdSidenav, $cookies, $http, StateService) {
        var self = this;

        self.toggle = function () {
            $mdSidenav('left').toggle();
        }

        init();
        function init(){
            if(null == $cookies.get('token')){
                StateService.to.login();
            } else {

                $http.defaults.headers.common.Authorization = $cookies.get('token');
            }

            $http.get("app/user/profile")
                .then(function(response){
                    console.log(response);
                })
        }

        return self;
    }
})();
