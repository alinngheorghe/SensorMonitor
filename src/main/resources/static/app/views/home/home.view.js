/**
 * Created by Kernel on 11/15/2016.
 */

String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
        if (m == "{{") {
            return "{";
        }
        if (m == "}}") {
            return "}";
        }
        return args[n];
    });
};
(function () {
    angular.module('hs')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$http', 'NgMap', '$state', 'WeatherFactory'];
    function HomeController($http, NgMap, $state, WeatherFactory) {
        var self = this;

        NgMap.getMap().then(function (map) {
        });

        self.data = {
            weather: {},
            house: {address: {}}
        };

        self.action = {
            get: get,
            save: save,
            toSensorList: toSensorList
        };

        init();
        function init() {
            get();
        }

        function toSensorList() {
            $state.go('main.sensors', {houseId: self.data.house.id, house: self.data.house});
        }

        function get() {
            $http.get("/api/house/v1/get")
                .then(function (response) {
                    self.data.house = response.data;
                    WeatherFactory.get(self.data.house.address.latitude, self.data.house.address.longitude)
                        .then(function (response) {
                            self.data.weather = response;
                        })
                });
        }

        function save() {
            $http.post("/api/house/v1/save", self.data.house)
                .then(function (response) {
                })
        }

        return self;
    }
})();
