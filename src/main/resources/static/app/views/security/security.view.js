/**
 * Created by Kernel on 11/26/2016.
 */
(function () {
    angular.module('hs')
        .controller('SecurityController', SecurityController);

    SecurityController.$inject = ['$cookies', '$httpParamSerializer', '$http', '$state', 'OAuthService'];
    function SecurityController($cookies, $httpParamSerializer, $http, $state, OAuthService) {
        var self = this;
        self.action= {
            login : OAuthService.login,
            register:  OAuthService.register
        };

        self.flag= {
            login : true
        }

        return self;
    }
})();