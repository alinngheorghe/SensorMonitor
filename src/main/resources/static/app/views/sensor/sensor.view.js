/**
 * Created by Kernel on 11/15/2016.
 */
(function () {
    angular.module('hs')
        .controller('SensorController', SensorController);

    SensorController.$inject = ['$http', '$stateParams', 'SensorDataFactory', 'ServerValueDataFactory', '$timeout', '$mdDialog'];
    function SensorController($http, $stateParams, SensorDataFactory, ServerValueDataFactory, $timeout, $mdDialog) {
        var self = this;
        self.data = {
            icon: 'arrow_back',
            sensor: {},
            selected: [],
            filter: {pageNo: 1, pageSize: 10},
            chart: {
                api: {},
                data: [],
                options: {}
            }
        };
        self.flags = {fullChart: false};
        self.action = {
            save: save,
            search: search,
            showTable: showTable,
            buildChart: buildChart,
            updateStatus: updateStatus
        };
        init();

        function init() {
            self.data.houseId = $stateParams.houseId;
            self.data.sensorId = $stateParams.sensorId;
            self.data.filter.sensorId = $stateParams.sensorId;
            self.data.chart.options = {
                chart: {
                    deepWatchData: false,
                    type: 'lineChart',
                    height: 450,
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 40,
                        left: 55
                    },
                    x: function (d) {
                        return d.x;
                    },
                    y: function (d) {
                        return d.y;
                    },
                    xAxis: {
                        axisLabel: 'Timestamp',
                        tickFormat: function (d) {
                            return moment(d).format('HH:mm:ss');
                        }
                    },
                    yAxis: {
                        axisLabel: 'Value',
                        tickFormat: function (d) {
                            return d3.format('.02f')(d);
                        },
                        axisLabelDistance: -10
                    }
                }
            };
            getCatalog();
            search();
            SensorDataFactory.findById(self.data.sensorId)
                .then(function (response) {
                    self.data.sensor = response.data;
                    switch (response.data.status) {
                        case 'STAND BY': {
                            self.statusColor = '#0000ff';
                            break;
                        }
                        case 'ACTIVE': {
                            self.statusColor = '#00ff00';
                            break;
                        }
                        case 'STOPPED': {
                            self.statusColor = '#ff0000';
                            break;
                        }
                    }
                });
        }

        function getCatalog() {
            $http.get('frontend/catalog/v1/getAll').then(function (response) {
                self.data.catalog = response.data;
            });
        }

        function search() {
            ServerValueDataFactory.findByFilter(self.data.filter)
                .then(function (response) {
                    self.data.records = response.data;
                    buildChart();
                });
        }

        function buildChart() {
            ServerValueDataFactory.findByFilterUnpaged(self.data.filter)
                .then(function (response) {
                    self.data.chart.data = [{
                        values: response.data,
                        key: 'Value',
                        color: '#ff7f0e'
                    }];
                });
        }

        function showTable() {
            self.flags.fullChart = !self.flags.fullChart;
            self.data.icon = self.flags.fullChart ? 'arrow_forward' : 'arrow_back';
            $timeout(function () {
                window.dispatchEvent(new Event('resize'));
            }, 600);
        }

        function save() {
            SensorDataFactory.save(self.data.sensor).then(function (response) {
            });
        }

        function updateStatus() {
            $mdDialog
                .show({
                    parent: angular.element(document.querySelector('md-content')),
                    templateUrl: 'app/views/sensor-list/sensor-status.dialog.html',
                    controllerAs: 'vm',
                    bindToController: true,
                    controller: 'SensorStatusController'
                })
                .then(function (response) {
                    if (null != response) {
                        var request = {sensors: self.data.sensor.id, status: response};
                        SensorDataFactory.updateStatus(request).then(function (response) {
                            search();
                        });
                    }
                });
        }

        function updateData() {
            $timeout(function () {
                search();
                updateData();
            }, 30000);
        }

        updateData();
        return self;
    }
})();
