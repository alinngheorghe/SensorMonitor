/**
 * Created by Alin N. Gheorghe on 15.12.2016.
 */
(function () {
    'use strict';

    angular.module('hs')
        .controller('SensorComparisonController', SensorComparisonController);

    SensorComparisonController.$inject = ['locals', '$mdDialog', 'ServerValueDataFactory'];
    function SensorComparisonController(locals, $mdDialog, ServerValueDataFactory) {
        var self = this;
        self.data = {
            chart: {
                api: {},
                data: [],
                options: {}
            }
        };
        self.action = {
            close: close
        };


        init();

        function init() {
            self.data.chart.options = {
                chart: {
                    deepWatchData: false,
                    type: 'lineChart',
                    height: 450,
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 40,
                        left: 55
                    },
                    x: function (d) {
                        return d.x;
                    },
                    y: function (d) {
                        return d.y;
                    },
                    xAxis: {
                        axisLabel: 'Timestamp',
                        tickFormat: function (d) {
                            return moment(d).format('HH:mm:ss');
                        }
                    },
                    yAxis: {
                        axisLabel: 'Value',
                        tickFormat: function (d) {
                            return d3.format('.02f')(d);
                        },
                        axisLabelDistance: -10
                    }
                }
            };
            ServerValueDataFactory.findByIds(_.map(locals, function (sensor) {
                return sensor.id
            }))
                .then(function (response) {
                    self.data.chart.data = [];
                    _.forEach(response.data, function (value, key) {
                        self.data.chart.data.push({
                            values: value,
                            key: key,
                            color: getRandomColor()
                        });
                    })

                })
        }

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function close() {
            $mdDialog.cancel();
        }

        return self;
    }
})();
