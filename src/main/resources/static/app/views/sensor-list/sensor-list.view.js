/**
 * Created by Kernel on 11/15/2016.
 */
(function () {
    angular.module('hs')
        .controller('SensorListController', SensorListController);

    SensorListController.$inject = ['$http', 'SensorDataFactory', 'ServerValueDataFactory', '$mdDialog', '$stateParams', '$state'];
    function SensorListController($http, SensorDataFactory, ServerValueDataFactory, $mdDialog, $stateParams, $state) {
        var self = this;
        self.data = {
            sensors: [],
            selected: [],
            filter: {}
        };

        self.flag = {search: false};
        self.isOpen = false;
        self.isStatusOpen = false;
        self.canBeopen = false;

        self.action = {
            add: add,
            edit: edit,
            search: search,
            remove: remove,
            changeStatus: changeStatus,
            backHome: backHome,
            compare: compare
        };

        self.catalog = {};

        function backHome() {
            $state.go('main.home');
        }

        function changeStatus(sensors) {
            $mdDialog
                .show({
                    parent: angular.element(document.querySelector('md-content')),
                    templateUrl: 'app/views/sensor-list/sensor-status.dialog.html',
                    controllerAs: 'vm',
                    bindToController: true,
                    controller: 'SensorStatusController'
                })
                .then(function (response) {
                    if (null != response) {
                        var request = {
                            sensors: _.map(sensors, function (sensor) {
                                return sensor.id
                            }), status: response
                        };
                        SensorDataFactory.updateStatus(request).then(function (response) {
                            search();
                        });
                    }
                });
        }

        function edit(sensor) {
            $mdDialog
                .show({
                    parent: angular.element(document.querySelector('md-content')),
                    templateUrl: 'app/components/sensor-form/sensor-form.html',
                    locals: sensor,
                    controllerAs: 'vm',
                    bindToController: true,
                    controller: 'SensorFormController'
                })
                .then(function (response) {
                });
        }

        function add($event) {
            var sensor = {houseId: 1};
            $mdDialog
                .show({
                    parent: angular.element(document.querySelector('md-content')),
                    targetEvent: $event,
                    templateUrl: 'app/components/sensor-form/sensor-form.html',
                    locals: sensor,
                    controllerAs: 'vm',
                    bindToController: true,
                    controller: 'SensorFormController'
                })
                .then(function (response) {
                    if (null != response) {
                        self.data.sensors.push(response);
                    }
                });
        }

        function remove() {
            var sensors = [];
            _.forEach(self.data.selected, function (selected) {
                sensors.push(selected.id);
                self.data.sensors.splice(self.data.sensors.indexOf(selected), 1);
            });
            SensorDataFactory.remove(sensors);
        }

        function search() {
            SensorDataFactory.findByFilter(self.data.filter)
                .then(function (response) {
                    self.data.sensors = response.data;
                });
        }

        function getCatalog() {
            $http.get('frontend/catalog/v1/getAll').then(function (response) {
                self.data.catalog = response.data;
            });
        }

        init();
        function init() {
            self.data.houseId = $stateParams.houseId;
            self.data.filter = {houseId: $stateParams.houseId, pageNo: 1, pageSize: 10};
            SensorDataFactory.findByFilter(self.data.filter)
                .then(function (response) {
                    self.data.sensors = response.data;
                });

            getCatalog();
            function get() {
                $http.get("/frontend/house/v1/get")
                    .then(function (response) {
                        self.data.house = response.data;
                    });
            }
        }

        function compare(toCompare) {
            $mdDialog
                .show({
                    parent: angular.element(document.querySelector('md-content')),
                    templateUrl: 'app/views/sensor-list/sensor-comparison.dialog.html',
                    locals: toCompare,
                    controllerAs: 'vm',
                    bindToController: true,
                    controller: 'SensorComparisonController'
                })
                .then(function (response) {
                });
        }

        return self;
    }
})();
