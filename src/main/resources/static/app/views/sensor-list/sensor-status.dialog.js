/**
 * Created by Alin N. Gheorghe on 15.12.2016.
 */
(function () {
    'use strict';

    angular.module('hs')
        .controller('SensorStatusController', SensorStatusController);

    SensorStatusController.$inject = ['$http', '$mdDialog'];
    function SensorStatusController($http, $mdDialog) {
        var self = this;
        self.data = {};
        self.action = {
            save: save,
            close: close
        };


        init();

        function init(){
            $http.get('frontend/catalog/v1/getStatusCatalog')
                .then(function(response){
                    console.log(response);
                    self.data.states = response.data;
                });
        }
        self.data = {
            states: []
        };

        function save() {
            $mdDialog.hide(self.data.state);
        }

        function close() {
            $mdDialog.cancel();
        }

        return self;
    }
})();
