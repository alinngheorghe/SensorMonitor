/**
 * Created by Kernel on 11/16/2016.
 */
class WeatherObject{
    constructor(date, humidity, pressure, temp, minTemp, maxTemp, windSpeed) {
        this._date = date;
        this._humidity = humidity;
        this._pressure = parseFloat(Math.round(pressure * 100) / 100).toFixed(2);
        this._temp = parseFloat(Math.round(temp * 100) / 100).toFixed(2);
        this._minTemp = parseFloat(Math.round(minTemp * 100) / 100).toFixed(2);
        this._maxTemp = parseFloat(Math.round(maxTemp * 100) / 100).toFixed(2);

        this.windSpeed = windSpeed;
    }

    get date() {
        return this._date;
    }

    set date(value) {
        this._date = value;
    }

    get humidity() {
        return this._humidity;
    }

    set humidity(value) {
        this._humidity = value;
    }

    get pressure() {
        return this._pressure;
    }

    set pressure(value) {
        this._pressure = value;
    }

    get temp() {
        return this._temp;
    }

    set temp(value) {
        this._temp = value;
    }

    get minTemp() {
        return this._minTemp;
    }

    set minTemp(value) {
        this._minTemp = value;
    }

    get maxTemp() {
        return this._maxTemp;
    }

    set maxTemp(value) {
        this._maxTemp = value;
    }
}


