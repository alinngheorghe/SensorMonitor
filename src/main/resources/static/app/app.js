/**
 * Created by Kernel on 11/13/2016.
 */


angular.module('hs', ['ui.router', 'ngMaterial', 'md.data.table', 'ngMap', 'Security', 'nvd3', 'ngCookies', 'ngMaterialDatePicker'])
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise('/home');
        $stateProvider
            .state('main', {
                url: '/',
                abstract: true,
                controllerAs: 'vm',
                controller: 'MainController',
                templateUrl: 'app/views/main/main.html?v=' + (new Date()).getTime()
            })
            .state('security', {
                url: '/security',
                controllerAs: 'vm',
                controller: 'SecurityController',
                templateUrl: 'app/views/security/security.html?v=' + (new Date()).getTime()
            })
            .state('main.home', {
                url: 'home',
                controllerAs: 'vm',
                controller: 'HomeController',
                templateUrl: 'app/views/home/home.view.html?v=' + (new Date()).getTime()
            })
            .state('main.sensors', {
                url: ':houseId/sensor-list',
                controllerAs: 'vm',
                controller: 'SensorListController',
                templateUrl: 'app/views/sensor-list/sensor-list.view.html?v=' + (new Date()).getTime()
            })
            .state('main.sensor-values', {
                url: 'sensor-value-list',
                controllerAs: 'vm',
                controller: 'SensorListController',
                templateUrl: 'app/views/sensor-value-list/sensor-value-list.view.html?v=' + (new Date()).getTime()
            })
            .state('main.sensor', {
                url: ':houseId/sensor/:sensorId',
                controllerAs: 'vm',
                controller: 'SensorController',
                templateUrl: 'app/views/sensor/sensor.view.html?v=' + (new Date()).getTime()
            });

        $httpProvider.interceptors.push('APIInterceptor');
    })
    .service('APIInterceptor', function ($cookies) {
        var service = this;
        service.request = function (config) {
            if (config.url.indexOf("http") == -1) {
                var token = $cookies.get('token');
                config.headers['X-Auth-Token'] = token;
            } else {
                config.headers = {
                    Accept: "application/json, text/plain, */*",
                    'Content-Type': 'application/json'
                };
            }
            return config;
        };
        service.responseError = function (response) {
            return response;
        };
    });
