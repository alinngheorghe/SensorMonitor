package com.kkraken.service;

import com.kkraken.bean.response.HouseData;
import com.kkraken.bean.response.UserData;
import com.kkraken.repository.UserRepository;
import com.kkraken.repository.dao.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Kernel on 11/27/2016.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HouseService houseService;

    public User register(UserData user) {
        BCryptPasswordEncoder encode = new BCryptPasswordEncoder();
        User newUser = userRepository.saveAndFlush(User.Builder.anUser()
                .withUsername(user.getUsername())
                .withPassword(encode.encode(user.getPassword()))
                .withEmail(user.getEmail())
                .withName(user.getName())
                .withActivated(true)
                .build());

        houseService.save(
                HouseData.Builder.aHouseResponse()
                        .withInterval(5L)
                        .withName(user.getUsername() + "'s house")
                        .withUserId(newUser.getId())
                        .build()
        );

        return newUser;
    }
}
