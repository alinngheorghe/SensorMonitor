package com.kkraken.service;

import com.kkraken.bean.convertors.AddressConvertor;
import com.kkraken.bean.response.AddressData;
import com.kkraken.repository.AddressRepository;
import com.kkraken.repository.dao.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by Kernel on 11/22/2016.
 */
@Service
public class AddressService {

    public static final String SYSTEM = "system";
    @Autowired
    private AddressRepository addressRepository;

    public void save(AddressData addressData) {
        Address inDB = null;

        if(null != addressData.getId()){
            inDB = addressRepository.findOne(addressData.getId());
        }
        inDB = addressRepository.saveAndFlush(
                AddressConvertor.map(addressData)
                        .withCreatedDate(null == inDB ? new Date() : inDB.getCreatedDate())
                        .withCreatedBy(SYSTEM)
                        .withUpdatedDate(null == inDB ? new Date() : inDB.getUpdatedDate())
                        .withUpdatedBy(SYSTEM)
                        .build()
        );
    }

    public com.kkraken.repository.dao.Address findByHouseId(long houseId) {
        return addressRepository.findByHouseId(houseId);
    }
}
