package com.kkraken.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

/**
 * Created by Kernel on 11/27/2016.
 */
@Component
public class ContextService {

    public String getLoggedUser() {
        Authentication a = SecurityContextHolder.getContext().getAuthentication();
        if(a.getPrincipal() instanceof String){
            return null;
        }
        User principal = (User) a.getPrincipal();
        return principal.getUsername();
    }
}
