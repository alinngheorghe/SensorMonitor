package com.kkraken.service;

import com.kkraken.bean.filters.SensorValueFilter;
import com.kkraken.bean.response.ChartValue;
import com.kkraken.bean.response.PagedResponse;
import com.kkraken.bean.response.RecordValue;
import com.kkraken.bean.response.SensorRecordsResponse;
import com.kkraken.bean.specification.SearchCriteria;
import com.kkraken.bean.specification.SpecificationBuilder;
import com.kkraken.repository.SensorValueRepository;
import com.kkraken.repository.dao.Sensor;
import com.kkraken.repository.dao.SensorValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Kernel on 11/21/2016.
 */
@Service
public class SensorValueService {
    @Autowired
    private SensorValueRepository sensorValueRepository;
    @Autowired
    private SpecificationBuilder<SensorValueFilter> specificationBuilder;

    public SensorValue save(SensorValue sensorValue) {
        return sensorValueRepository.saveAndFlush(sensorValue);
    }

    public SensorValue findById(Long id) {
        return sensorValueRepository.findOne(id);
    }

    public List<SensorValue> findBySensorId(Long id) {
        return sensorValueRepository.findBySensorId(id);
    }

    public PagedResponse<RecordValue> findByFilter(SensorValueFilter filter) {
        Page<SensorValue> results = sensorValueRepository.findAll(buildSpecification(filter), new PageRequest(filter.getPageNo() - 1, filter.getPageSize()));
        PagedResponse<RecordValue> response = PagedResponse.Builder.aPagedResponse()
            .withPageTotal(results.getTotalPages())
            .withResultTotal(results.getTotalElements())
            .withResultList(results.getContent().stream()
                .map(result ->
                    RecordValue.Builder.aRecordValue()
                        .withId(result.getId())
                        .withUnit(result.getUnit())
                        .withValue(result.getValue())
                        .withTakenAt(result.getTakenAt().toString())
                        .build())
                .collect(Collectors.toList()))
            .build();
        return response;
    }

    public void clear() {
        sensorValueRepository.deleteAll();
    }

    private Specification<SensorValue> buildSpecification(SensorValueFilter filter) {
        Specifications<SensorValue> specifications = null;
        if (null != filter.getId()) {
            specifications = specificationBuilder.andIfNotNull(specifications, new SearchCriteria("id", ":", String.valueOf(filter.getId())));
        }
        if (null != filter.getSensorId()) {
            specifications = specificationBuilder.andIfNotNull(specifications, new SearchCriteria("sensorId", ":", String.valueOf(filter.getSensorId())));
        }
        if (null != filter.getValueFrom()) {
            specifications = specificationBuilder.andIfNotNull(specifications, new SearchCriteria("value", ">", String.valueOf(filter.getValueFrom())));
        }

        if (null != filter.getValueTo()) {
            specifications = specificationBuilder.andIfNotNull(specifications, new SearchCriteria("value", ">", String.valueOf(filter.getValueTo())));
        }

        if (null != filter.getTakenAtFrom()) {
            specifications = specificationBuilder.andIfNotNull(specifications, new SearchCriteria("takenAt", ">", String.valueOf(filter.getTakenAtFrom())));
        }

        if (null != filter.getTakenAtTo()) {
            specifications = specificationBuilder.andIfNotNull(specifications, new SearchCriteria("takenAt", ">", String.valueOf(filter.getTakenAtTo())));
        }

        if (null != filter.getUnit()) {
            specifications = specificationBuilder.andIfNotNull(specifications, new SearchCriteria("unit", ":", String.valueOf(filter.getUnit())));
        }
        return specifications;
    }

    public List<ChartValue> findByFilterUnpaged(SensorValueFilter filter) {
        List<SensorValue> values = sensorValueRepository.findTop200BySensorIdOrderByTakenAt(filter.getSensorId());
        return values.stream().map(value -> new ChartValue(value.getTakenAt().getTime(), value.getValue())).collect(Collectors.toList());
    }

    public Map<Long, List<ChartValue>> findBySensorList(List<Long> sensorIdList) {
        Map<Long, List<ChartValue>> result = new HashMap<Long, List<ChartValue>>();
        sensorIdList.stream().forEach(id -> result.put(id, sensorValueRepository.findTop100BySensorIdOrderByTakenAtDesc(id).stream().map(value -> new ChartValue(value.getTakenAt().getTime(), value.getValue())).collect(Collectors.toList())));
        return result;
    }
}
