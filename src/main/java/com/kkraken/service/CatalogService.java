package com.kkraken.service;

import com.kkraken.bean.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Alin N. Gheorghe on 17.12.2016.
 */
@Service
public class CatalogService {

    public Map<String, List<Item>> getAll(){
        Map<String, List<Item>> response = new HashMap<String, List<Item>>();
        response.put("status", getStatusCatalog());
        response.put("sensorTypes", sensorTypes());
        response.put("sensorUnits", sensorUnits());

        return response;
    }

    public List<Item> getStatusCatalog(){
        List<Item> items = new ArrayList<Item>();
        items.add(new Item("", ""));
        items.add(new Item("ACTIVE", "Active"));
        items.add(new Item("STOPPED", "Stopped"));
        return items;
    }

    public List<Item> sensorTypes(){
        List<Item> items = new ArrayList<Item>();
        items.add(new Item("", ""));
        items.add(new Item("TEMP", "Temperature"));
        items.add(new Item("PRE", "Preasure"));
        items.add(new Item("HUM", "Humidity"));
        return items;
    }

    public List<Item> sensorUnits(){
        List<Item> items = new ArrayList<Item>();
        items.add(new Item("", ""));
        items.add(new Item("C", "Celsius"));
        items.add(new Item("PA", "Pascal"));
        items.add(new Item("PERC", "Percent"));
        return items;
    }

}
