package com.kkraken.service;

import com.kkraken.bean.convertors.AddressConvertor;
import com.kkraken.bean.response.AddressData;
import com.kkraken.bean.response.HouseData;
import com.kkraken.bean.response.UserData;
import com.kkraken.repository.HouseRepository;
import com.kkraken.repository.UserRepository;
import com.kkraken.repository.dao.Address;
import com.kkraken.repository.dao.House;
import com.kkraken.repository.dao.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Kernel on 11/21/2016.
 */
@Service
public class HouseService {
    public static final long HOUSE_ID = 1L;
    public static final String SYSTEM = "system";
    @Autowired
    private HouseRepository houseRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private ContextService contextService;

    public void save(HouseData houseData) {
        House inDB = null;
        if (null != houseData.getId()) {
            inDB = houseRepository.findOne(houseData.getId());
        }
        House house = House.Builder.aHouse()
                .withId(houseData.getId())
                .withUserId(houseData.getUserId())
                .withName(houseData.getName())
                .withInterval(houseData.getInterval())
                .withCreatedDate(null == inDB ? new Date() : inDB.getCreatedDate())
                .withCreatedBy(SYSTEM)
                .withUpdatedDate(null == inDB ? new Date() : inDB.getUpdatedDate())
                .withUpdatedBy(SYSTEM)
                .build();
        house = houseRepository.saveAndFlush(house);
        if(null == houseData.getAddress()){
            houseData.setAddress(new AddressData());
        }
        houseData.getAddress().setHouseId(house.getId());
        addressService.save(houseData.getAddress());
    }

    @Autowired
    private UserRepository userRepository;

    public HouseData getHouse() {
        String username = contextService.getLoggedUser();
        if(null == username){
            return null;
        }
        User user = userRepository.findByUsername(username);
        List<House> house = houseRepository.findByUserId(user.getId());
        Address address = addressService.findByHouseId(house.get(0).getId());
        return
                HouseData.Builder.aHouseResponse()
                        .withId(house.get(0).getId())
                        .withUserId(house.get(0).getUserId())
                        .withName(house.get(0).getName())
                        .withInterval(house.get(0).getInterval())
                        .withAddress(AddressConvertor.map(address).build())
                        .build();
    }
}
