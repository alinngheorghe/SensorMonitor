package com.kkraken.service;

import com.kkraken.bean.UpdateSensorStatusRequest;
import com.kkraken.bean.convertors.SensorConvertor;
import com.kkraken.bean.filters.SensorFilter;
import com.kkraken.bean.filters.SensorValueFilter;
import com.kkraken.bean.response.SensorData;
import com.kkraken.bean.specification.SearchCriteria;
import com.kkraken.bean.specification.SensorSpecification;
import com.kkraken.bean.specification.SpecificationBuilder;
import com.kkraken.repository.SensorRepository;
import com.kkraken.repository.dao.Sensor;
import com.kkraken.repository.dao.SensorValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Kernel on 11/21/2016.
 */
@Service
public class SensorService {
    public static final String SYSTEM = "system";
    @Autowired
    private SensorRepository sensorRepository;
    @Autowired
    private SpecificationBuilder<Sensor> specificationBuilder;
    @Autowired
    private SensorValueService sensorValueService;

    @Autowired
    private ContextService contextService;

    public SensorData save(SensorData sensorData) {
        Sensor inDB = null;

        if (null != sensorData.getId()) {
            inDB = sensorRepository.findOne(sensorData.getId());
        }

        Sensor sensor = SensorConvertor.map(sensorData)
                .withCreatedDate(null == inDB ? new Date() : inDB.getCreatedDate())
                .withCreatedBy(contextService.getLoggedUser())
                .withUpdatedDate(null == inDB ? new Date() : inDB.getUpdatedDate())
                .withUpdatedBy(contextService.getLoggedUser())
                .withStatus(null == inDB ? "STOPPED" : inDB.getStatus())
                .build();
        return SensorConvertor.map(sensorRepository.saveAndFlush(sensor)).build();
    }

    public Sensor findById(Long id) {
        return sensorRepository.findOne(id);
    }

    @Autowired
    private CatalogService catalogService;

    public List<SensorData> findByFilter(SensorFilter filter) {
        Specifications<Sensor> specifications = null;
        specifications =
                Specifications.where(new SensorSpecification(new SearchCriteria("houseId", ":", String.valueOf(filter.getHouseId()))))
                        .and(null != filter.getId() ? new SensorSpecification( new SearchCriteria("id", ":", String.valueOf(filter.getId()))) : null)
                        .and(null != filter.getType() && !filter.getType().isEmpty() ? new SensorSpecification(new SearchCriteria("type", ":", String.valueOf(filter.getType()))): null)
                        .and(null != filter.getUnit() && !filter.getUnit().isEmpty() ? new SensorSpecification(new SearchCriteria("unit", ":", String.valueOf(filter.getUnit()))): null)
                        .and(null != filter.getStatus() && !filter.getStatus().isEmpty() ? new SensorSpecification(new SearchCriteria("status", ":", String.valueOf(filter.getStatus()))): null);
        Page<Sensor> result = sensorRepository.findAll(specifications, new PageRequest(filter.getPageNo() - 1, filter.getPageSize()));
        return result.getContent().stream().map(sensor ->
                SensorData.Builder.aSensorData()
                        .withUnit((String) catalogService.sensorUnits().stream().filter(unit -> unit.getKey().equals(sensor.getUnit())).findFirst().get().getDescription())
                        .withType((String) catalogService.sensorTypes().stream().filter(unit -> unit.getKey().equals(sensor.getType())).findFirst().get().getDescription())
                        .withStatus((String) catalogService.getStatusCatalog().stream().filter(unit -> unit.getKey().equals(sensor.getStatus())).findFirst().get().getDescription())
                        .withId(sensor.getId())
                        .withName(sensor.getName())
                        .withHouseId(sensor.getHouseId())
                        .withInterval(sensor.getInterval())
                        .withCreatedBy(sensor.getCreatedBy())
                        .withCreatedDate(sensor.getCreatedDate())
                        .build())
                .collect(Collectors.toList());
    }

    @Transactional
    public void delete(List<Long> sensorIds) {
        sensorIds.stream().forEach(sensor -> sensorRepository.delete(sensor));
    }

    public void updateStatus(UpdateSensorStatusRequest request) {
        List<Sensor> sensors = sensorRepository.findByIdIn(request.getSensors());
        sensors.stream().forEach(sensor -> sensor.setStatus(request.getStatus()));
        sensorRepository.save(sensors);
    }

    public List<Sensor> findByStatus() {
        return sensorRepository.findByStatus("ACTIVE");
    }
}
