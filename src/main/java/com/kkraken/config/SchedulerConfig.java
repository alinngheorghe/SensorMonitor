package com.kkraken.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by Kernel on 11/25/2016.
 */
@Configuration
@EnableScheduling
public class SchedulerConfig{}