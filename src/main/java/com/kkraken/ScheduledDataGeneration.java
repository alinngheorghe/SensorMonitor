package com.kkraken;

import com.kkraken.repository.dao.Sensor;
import com.kkraken.repository.dao.SensorValue;
import com.kkraken.service.SensorService;
import com.kkraken.service.SensorValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Kernel on 11/25/2016.
 */
@Component
public class ScheduledDataGeneration {
    @Autowired
    private SensorValueService sensorValueService;
    @Autowired
    private SensorService sensorService;

    private void generateData(Sensor sensor) {
        long range = 50;
        Random random = new Random();
        Calendar cal = Calendar.getInstance();

        sensorValueService.save(
                SensorValue.Builder.sensorValue()
                        .withValue((long) (random.nextDouble() * range))
                        .withSensorId(sensor.getId()).withUnit(sensor.getUnit()).withTakenAt(new Date()).build());
    }

    @Scheduled(fixedDelay = 30000)
    public void publishUpdates() {
        List<Sensor> sensors = sensorService.findByStatus();
        sensors.stream().forEach(sensor -> generateData(sensor));

    }
}
