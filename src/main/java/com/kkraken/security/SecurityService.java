package com.kkraken.security;

/**
 * Created by Alin N. Gheorghe on 08.12.2016.
 */
public interface SecurityService {

    public Boolean hasProtectedAccess();

}
