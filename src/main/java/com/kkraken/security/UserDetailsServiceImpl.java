package com.kkraken.security;

import com.kkraken.repository.UserRepository;
import com.kkraken.repository.dao.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Alin N. Gheorghe on 08.12.2016.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return CerberusUser.Builder.aCerberusUser()
                    .withId(user.getId())
                    .withUsername(user.getUsername())
                    .withPassword(user.getPassword())
                    .withEmail(user.getEmail())
                    .withAuthorities(AuthorityUtils.commaSeparatedStringToAuthorityList(user.getAuthorities()))
                    .build();
        }
    }

}
