package com.kkraken.controller;

import com.kkraken.bean.Item;
import com.kkraken.repository.dao.Sensor;
import com.kkraken.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by Alin N. Gheorghe on 17.12.2016.
 */
@RestController
@RequestMapping(value = "/frontend/catalog/")
public class CatalogController {
    @Autowired
    private CatalogService catalogService;

    @RequestMapping(value = "v1/getAll", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<Map<String, List<Item>>>(catalogService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/getStatusCatalog", method = RequestMethod.GET)
    public ResponseEntity<?> getStatusCatalog() {
        return new ResponseEntity<List<Item>>(catalogService.getStatusCatalog(), HttpStatus.OK);
    }
}
