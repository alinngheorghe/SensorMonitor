package com.kkraken.controller;

import com.kkraken.bean.filters.SensorFilter;
import com.kkraken.bean.filters.SensorValueFilter;
import com.kkraken.bean.response.ChartValue;
import com.kkraken.bean.response.PagedResponse;
import com.kkraken.bean.response.RecordValue;
import com.kkraken.repository.dao.Sensor;
import com.kkraken.repository.dao.SensorValue;
import com.kkraken.service.SensorService;
import com.kkraken.service.SensorValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Kernel on 11/21/2016.
 */
@RestController
@RequestMapping(value = "/frontend/sensor-value/")
public class SensorValueController {

    @Autowired
    private SensorValueService sensorValueService;

    @RequestMapping(value = "v1/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> get(@PathVariable Long id) {
        return new ResponseEntity<SensorValue>(sensorValueService.findById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/get/bySensorIds", method = RequestMethod.POST)
    public ResponseEntity<?> getByIdList(@RequestBody List<Long> sensorIdList){
        return new ResponseEntity<Map<Long,List<ChartValue>>>(sensorValueService.findBySensorList(sensorIdList), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/getByFilter", method = RequestMethod.POST)
    public ResponseEntity<?> getByFilter(@RequestBody SensorValueFilter filter) {
        return new ResponseEntity<PagedResponse<RecordValue>>(sensorValueService.findByFilter(filter), HttpStatus.OK);
    }
    @RequestMapping(value = "v1/findByFilterUnpaged", method = RequestMethod.POST)
    public ResponseEntity<?> findByFilterUnpaged(@RequestBody SensorValueFilter filter) {
        return new ResponseEntity<List<ChartValue>>(sensorValueService.findByFilterUnpaged(filter), HttpStatus.OK);
    }
}
