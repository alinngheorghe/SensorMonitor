package com.kkraken.controller;

import com.kkraken.bean.Item;
import com.kkraken.bean.response.UserData;
import com.kkraken.bean.response.UserProfile;
import com.kkraken.repository.dao.User;
import com.kkraken.service.ContextService;
import com.kkraken.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Kernel on 11/27/2016.
 */
@RestController
@RequestMapping(value = "/user/")
public class UserController {

    @Autowired
    private ContextService contextService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "profile", method = RequestMethod.GET)
    public ResponseEntity<String> profile(@PathVariable String username){
        return new ResponseEntity<String>(contextService.getLoggedUser(), HttpStatus.OK);
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public ResponseEntity<User> profile(@RequestBody UserData user){
        return new ResponseEntity<User>(userService.register(user), HttpStatus.OK);
    }


}
