package com.kkraken.controller;

import com.kkraken.bean.Item;
import com.kkraken.bean.response.HouseData;
import com.kkraken.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Kernel on 11/21/2016.
 */
@RestController
@RequestMapping(value = "/api/house/")
public class HouseController {
    @Autowired
    private HouseService houseService;

    @RequestMapping(value = "v1/get", method = RequestMethod.GET)
    public ResponseEntity<?> getHouse(){
        return new ResponseEntity<HouseData>(houseService.getHouse(), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveHouse(@RequestBody HouseData house){
        houseService.save(house);
        return response();
    }
    public ResponseEntity<Item> response(){
        return new ResponseEntity<Item>(new Item("status", "OK"), HttpStatus.OK);
    }
}
