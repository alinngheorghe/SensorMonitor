package com.kkraken.controller;

import com.kkraken.bean.Item;
import com.kkraken.bean.UpdateSensorStatusRequest;
import com.kkraken.bean.filters.SensorFilter;
import com.kkraken.bean.response.SensorData;
import com.kkraken.repository.dao.Sensor;
import com.kkraken.service.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Kernel on 11/21/2016.
 */
@RestController
@RequestMapping(value = "/frontend/sensor/")
public class SensorController {

    @Autowired
    private SensorService sensorService;

    @RequestMapping(value = "v1/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getSensor(@PathVariable Long id) {
        return new ResponseEntity<Sensor>(sensorService.findById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/save", method = RequestMethod.POST)
    public ResponseEntity<?> save(@RequestBody SensorData sensor) {
        return new ResponseEntity<SensorData>(sensorService.save(sensor), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/updateStatus", method = RequestMethod.POST)
    public ResponseEntity<?> updateStatus(@RequestBody UpdateSensorStatusRequest request) {
        sensorService.updateStatus(request);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "v1/delete", method = RequestMethod.POST)
    public ResponseEntity<?> delete(@RequestBody List<Long> sensorIds) {
        sensorService.delete(sensorIds);
        return response();
    }

    @RequestMapping(value = "v1/getByFilter", method = RequestMethod.POST)
    public ResponseEntity<?> getHouse(@RequestBody SensorFilter filter) {
        return new ResponseEntity<List<SensorData>>(sensorService.findByFilter(filter), HttpStatus.OK);
    }



    public ResponseEntity<Item> response() {
        return new ResponseEntity<Item>(new Item("status", "OK"), HttpStatus.OK);
    }
}
