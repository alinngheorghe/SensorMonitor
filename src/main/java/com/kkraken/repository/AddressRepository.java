package com.kkraken.repository;

import com.kkraken.repository.dao.Address;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Kernel on 11/21/2016.
 */
public interface AddressRepository  extends JpaRepository<Address, Long>{
    Address findByHouseId(Long houseId);
}
