package com.kkraken.repository.dao;

import javax.persistence.*;


/**
 * Created by Kernel on 11/17/2016.
 */

@Entity
@Table(name = "RELATIONSHIP")
public class Relationship extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FROM_ID")
    private String fromId;

    @Column(name = "FROM_TYPE")
    private String fromType;

    @Column(name = "TO_ID")
    private String toId;

    @Column(name = "TO_TYPE")
    private String toType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getToType() {
        return toType;
    }

    public void setToType(String toType) {
        this.toType = toType;
    }

    public static class Builder extends BaseEntity.Builder<Relationship, Builder> {

        private Builder() {
            super(new Relationship());
        }

        public static Builder relationship() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.entity.setId(id);
            return this;
        }

        public Builder withFromId(String fromId) {
            this.entity.setFromId(fromId);
            return this;
        }

        public Builder withFromType(String fromType) {
            this.entity.setFromType(fromType);
            return this;
        }

        public Builder withToId(String toId) {
            this.entity.toId = toId;
            return this;
        }

        public Builder withToType(String toType) {
            this.entity.toType = toType;
            return this;
        }
    }
}
