package com.kkraken.repository.dao;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Kernel on 11/17/2016.
 */
@Entity
@Table(name = "HOUSE")
public class House extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "BASE_INTERVAL")
    private Long interval;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getInterval() {
        return interval;
    }

    public void setInterval(Long interval) {
        this.interval = interval;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static class Builder extends BaseEntity.Builder<House, Builder> {
        private Builder() {
            super(new House());
        }

        public static Builder aHouse() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.entity.setId(id);
            return this;
        }
        public Builder withUserId(Long id) {
            this.entity.setUserId(id);
            return this;
        }

        public Builder withName(String name) {
            this.entity.setName(name);
            return this;
        }

        public Builder withInterval(Long interval) {
            this.entity.setInterval(interval);
            return this;
        }

    }
}
