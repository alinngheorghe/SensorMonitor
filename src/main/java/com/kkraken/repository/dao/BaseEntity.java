package com.kkraken.repository.dao;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Kernel on 11/17/2016.
 */
@MappedSuperclass
public abstract class BaseEntity {
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date updatedDate;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "UPDATE_BY")
    private String updatedBy;

    @Column(name = "VALID_TO")
    @Temporal(TemporalType.DATE)
    private Date validTo;


    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }


    public static class Builder<E extends BaseEntity, B extends Builder> {
        protected E entity;

        protected Builder(E entity) {
            this.entity = entity;
        }

        public B withCreatedDate(Date createdDate) {
            this.entity.setCreatedDate(createdDate);
            return (B) this;
        }

        public B withUpdatedDate(Date updatedDate) {
            this.entity.setUpdatedDate(updatedDate);
            return (B) this;
        }

        public B withCreatedBy(String createdBy) {
            this.entity.setCreatedBy(createdBy);
            return (B) this;
        }

        public B withUpdatedBy(String updatedBy) {
            this.entity.setUpdatedBy(updatedBy);
            return (B) this;
        }

        public B withValidTo(Date validTo) {
            this.entity.setValidTo(validTo);
            return (B) this;
        }

        public E build() {
            return this.entity;
        }
    }
}

