package com.kkraken.repository.dao;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Kernel on 11/17/2016.
 */
@Entity
@Table(name = "SENSOR_VALUE")
public class SensorValue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "SENSOR_ID", nullable = false)
    private Long sensorId;

    @Column(name = "VALUE", nullable = false)
    private Long value;

    @Column(name = "UNIT", nullable = false)
    private String unit;

    @Column(name = "TAKEN_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date takenAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSensorId() {
        return sensorId;
    }

    public void setSensorId(Long sensorId) {
        this.sensorId = sensorId;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Date getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(Date takenAt) {
        this.takenAt = takenAt;
    }

    public static class Builder {
        private SensorValue entity;

        private Builder() {
            this.entity = new SensorValue();
        }

        public static Builder sensorValue() {
            return new Builder();
        }

        public Builder withSensorId(Long sensorId) {
            this.entity.sensorId = sensorId;
            return this;
        }

        public Builder withValue(Long value) {
            this.entity.value = value;
            return this;
        }

        public Builder withUnit(String unit) {
            this.entity.unit = unit;
            return this;
        }

        public Builder withTakenAt(Date takenAt) {
            this.entity.takenAt = takenAt;
            return this;
        }

        public SensorValue build() {
            return this.entity;
        }
    }
}
