package com.kkraken.repository.dao;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Kernel on 11/17/2016.
 */
@Entity
@Table(name = "SENSOR")
public class Sensor extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "TYPE", nullable = false)
    private String type;

    @Column(name = "UNIT", nullable = false)
    private String unit;

    @Column(name = "STATUS", nullable = false)
    private String status;

    @Column(name = "RECORD_INTERVAL", nullable = false)
    private Long interval;

    @Column(name = "HOUSE_ID", nullable = false)
    private Long houseId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getInterval() {
        return interval;
    }

    public void setInterval(Long interval) {
        this.interval = interval;
    }

    public static class Builder extends BaseEntity.Builder<Sensor, Builder> {
        private Builder() {
            super(new Sensor());
        }

        public static Builder sensor() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.entity.setId(id);
            return this;
        }

        public Builder withName(String name) {
            this.entity.setName(name);
            return this;
        }

        public Builder withType(String type) {
            this.entity.setType(type);
            return this;
        }

        public Builder withUnit(String unit) {
            this.entity.setUnit(unit);
            return this;
        }

        public Builder withHouseId(Long houseId) {
            this.entity.setHouseId(houseId);
            return this;
        }

        public Builder withInterval(Long interval) {
            this.entity.setInterval(interval);
            return this;
        }

        public Builder withStatus(String status) {
            this.entity.setStatus(status);
            return this;
        }

        public Sensor build() {
            return this.entity;
        }
    }
}

