package com.kkraken.repository.dao;

import javax.persistence.*;

/**
 * Created by Kernel on 11/17/2016.
 */
@Entity
@Table(name = "ADDRESS")
public class Address extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "house_id", nullable = false)
    private Long houseId;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "CITY")
    private String city;

    @Column(name = "COUNTY")
    private String county;

    @Column(name = "LINE")
    private String addressLine;

    @Column(name = "LATITUDE")
    private Float latitude;

    @Column(name = "LONGITUDE")
    private Float longiude;

    public Address() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongiude() {
        return longiude;
    }

    public void setLongiude(Float longiude) {
        this.longiude = longiude;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public static class Builder extends BaseEntity.Builder<Address, Builder> {
        private Builder() {
            super(new Address());
        }

        public static Builder address() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.entity.setId(id);
            return this;
        }

        public Builder withHouseId(Long houseId) {
            this.entity.setHouseId(houseId);
            return this;
        }

        public Builder withCountry(String country) {
            this.entity.setCountry(country);
            return this;
        }

        public Builder withCity(String city) {
            this.entity.setCity(city);
            return this;
        }

        public Builder withCounty(String county) {
            this.entity.setCounty(county);
            return this;
        }

        public Builder withAddressLine(String addressLine) {
            this.entity.setAddressLine(addressLine);
            return this;
        }

        public Builder withLatitude(Float latitude) {
            this.entity.setLatitude(latitude);
            return this;
        }

        public Builder withLongiude(Float longiude) {
            this.entity.setLongiude(longiude);
            return this;
        }
    }
}
