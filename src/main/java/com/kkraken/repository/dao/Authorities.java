package com.kkraken.repository.dao;

/**
 * Created by Kernel on 11/20/2016.
 */
public enum Authorities {
    ROLE_USER,
    ROLE_ADMIN
}