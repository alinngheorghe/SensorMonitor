package com.kkraken.repository;

import com.kkraken.repository.dao.SensorValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by Kernel on 11/21/2016.
 */
public interface SensorValueRepository  extends JpaRepository<SensorValue, Long>, JpaSpecificationExecutor<SensorValue> {
    List<SensorValue> findBySensorId(Long sensorId);
    List<SensorValue> findTop200BySensorIdOrderByTakenAt(Long sensorId);
    List<SensorValue> findTop100BySensorIdOrderByTakenAtDesc(Long sensorId);
}
