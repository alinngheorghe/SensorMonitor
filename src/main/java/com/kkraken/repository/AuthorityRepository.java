package com.kkraken.repository;

/**
 * Created by Kernel on 11/20/2016.
 */

import com.kkraken.repository.dao.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, String> {
}