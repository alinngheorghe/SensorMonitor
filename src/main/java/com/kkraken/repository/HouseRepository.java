package com.kkraken.repository;

import com.kkraken.repository.dao.House;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Kernel on 11/21/2016.
 */
public interface HouseRepository extends JpaRepository<House, Long> {
    List<House> findByUserId(Long id);
}
