package com.kkraken.repository;

import com.kkraken.repository.dao.Relationship;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Kernel on 11/21/2016.
 */
public interface RelationshipRepository extends JpaRepository<Relationship, Long> {
}
