package com.kkraken.bean.exception;

/**
 * Created by Kernel on 11/17/2016.
 */
public class BaseException extends Exception {
    private String code;
    private Long timestamp;

    public BaseException(String message, String code) {
        super(message);
        this.code = code;
        this.timestamp = System.currentTimeMillis();
    }
}
