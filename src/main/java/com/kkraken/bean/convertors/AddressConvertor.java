package com.kkraken.bean.convertors;

import com.kkraken.bean.response.AddressData;
import com.kkraken.repository.dao.Address;

/**
 * Created by Kernel on 11/22/2016.
 */
public class AddressConvertor {

    public static AddressData.Builder map(Address address) {
        return AddressData.Builder.anAddress()
                .withId(address.getId())
                .withHouseId(address.getHouseId())
                .withCountry(address.getCountry())
                .withCounty(address.getCounty())
                .withCity(address.getCity())
                .withAddressLine(address.getAddressLine())
                .withLatitude(address.getLatitude())
                .withLongitude(address.getLongiude());
    }

    public static Address.Builder map(AddressData addressData) {
        return Address.Builder.address()
                .withId(addressData.getId())
                .withHouseId(addressData.getHouseId())
                .withCountry(addressData.getCountry())
                .withCounty(addressData.getCounty())
                .withCity(addressData.getCity())
                .withAddressLine(addressData.getAddressLine())
                .withLatitude(addressData.getLatitude())
                .withLongiude(addressData.getLongitude());
    }
}
