package com.kkraken.bean.convertors;

import com.kkraken.bean.response.SensorData;
import com.kkraken.repository.dao.Sensor;

/**
 * Created by Kernel on 11/23/2016.
 */
public class SensorConvertor {
    public static SensorData.Builder map(Sensor sensor) {
        return SensorData.Builder.aSensorData()
                .withId(sensor.getId())
                .withHouseId(sensor.getHouseId())
                .withName(sensor.getName())
                .withInterval(sensor.getInterval())
                .withType(sensor.getType())
                .withUnit(sensor.getUnit())
                .withStatus(sensor.getStatus());
    }

    public static Sensor.Builder map(SensorData sensor) {
        return Sensor.Builder.sensor()
                .withId(sensor.getId())
                .withHouseId(sensor.getHouseId())
                .withName(sensor.getName())
                .withInterval(sensor.getInterval())
                .withType(sensor.getType())
                .withUnit(sensor.getUnit());
    }

}
