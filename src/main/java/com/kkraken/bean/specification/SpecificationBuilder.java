package com.kkraken.bean.specification;

import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

/**
 * Created by Kernel on 11/21/2016.
 */
@Component
public class SpecificationBuilder<T> {
    public Specifications andIfNotNull(Specifications specification, SearchCriteria criteria) {
        if (null == specification) {
            return Specifications.where(new SensorSpecification(criteria));
        }
        specification .and(new SensorSpecification(criteria));
        return specification;
    }
}
