package com.kkraken.bean.filters;

import java.util.Date;

/**
 * Created by Kernel on 11/21/2016.
 */
public class SensorValueFilter extends BaseFilter {
    private Long id;
    private Long sensorId;
    private String unit;
    private Long valueTo;
    private Long valueFrom;
    private Date takenAtTo;
    private Date takenAtFrom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSensorId() {
        return sensorId;
    }

    public void setSensorId(Long sensorId) {
        this.sensorId = sensorId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getValueTo() {
        return valueTo;
    }

    public void setValueTo(Long valueTo) {
        this.valueTo = valueTo;
    }

    public Long getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(Long valueFrom) {
        this.valueFrom = valueFrom;
    }

    public Date getTakenAtTo() {
        return takenAtTo;
    }

    public void setTakenAtTo(Date takenAtTo) {
        this.takenAtTo = takenAtTo;
    }

    public Date getTakenAtFrom() {
        return takenAtFrom;
    }

    public void setTakenAtFrom(Date takenAtFrom) {
        this.takenAtFrom = takenAtFrom;
    }


}
