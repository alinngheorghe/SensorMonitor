package com.kkraken.bean.filters;

/**
 * Created by Kernel on 11/21/2016.
 */
public class SensorFilter {
    private Long id;
    private Long houseId;
    private String type;
    private String unit;
    private String status;

    private Integer pageNo;
    private Integer pageSize;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public static final class Builder {
        private Long id;
        private Long houseId;
        private String type;
        private String unit;
        private String status;
        private Integer pageNo;
        private Integer pageSize;

        private Builder() {
        }

        public static Builder aSensorFilter() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withHouseId(Long houseId) {
            this.houseId = houseId;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Builder withUnit(String unit) {
            this.unit = unit;
            return this;
        }

        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder withPageNo(Integer pageNo) {
            this.pageNo = pageNo;
            return this;
        }

        public Builder withPageSize(Integer pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        public SensorFilter build() {
            SensorFilter sensorFilter = new SensorFilter();
            sensorFilter.setId(id);
            sensorFilter.setHouseId(houseId);
            sensorFilter.setType(type);
            sensorFilter.setUnit(unit);
            sensorFilter.setStatus(status);
            sensorFilter.setPageNo(pageNo);
            sensorFilter.setPageSize(pageSize);
            return sensorFilter;
        }
    }
}
