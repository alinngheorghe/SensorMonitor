package com.kkraken.bean;

import java.util.List;

/**
 * Created by Alin N. Gheorghe on 16.12.2016.
 */
public class UpdateSensorStatusRequest {
    private List<Long> sensors;
    private String status;

    public UpdateSensorStatusRequest() {
    }

    public UpdateSensorStatusRequest(List<Long> sensors, String status) {
        this.sensors = sensors;
        this.status = status;
    }

    public List<Long> getSensors() {
        return sensors;
    }

    public void setSensors(List<Long> sensors) {
        this.sensors = sensors;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
