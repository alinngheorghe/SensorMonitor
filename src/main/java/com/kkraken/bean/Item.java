package com.kkraken.bean;

import java.util.Objects;

/**
 * Created by Kernel on 11/21/2016.
 */
public class Item<K extends Object, V extends Object> {
    private K key;
    private V description;

    public Item(K key, V value) {
        this.key = key;
        this.description = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getDescription() {
        return description;
    }

    public void setDescription(V description) {
        this.description = description;
    }
}
