package com.kkraken.bean.response;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Alin N. Gheorghe on 16.12.2016.
 */
public class RecordValue implements Serializable{
    private Long id;
    private Long value;
    private String unit;
    private String takenAt;

    public RecordValue() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(String takenAt) {
        this.takenAt = takenAt;
    }

    public static final class Builder {
        private Long id;
        private Long value;
        private String unit;
        private String takenAt;

        private Builder() {
        }

        public static Builder aRecordValue() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withValue(Long value) {
            this.value = value;
            return this;
        }

        public Builder withUnit(String unit) {
            this.unit = unit;
            return this;
        }

        public Builder withTakenAt(String takenAt) {
            this.takenAt = takenAt;
            return this;
        }

        public RecordValue build() {
            RecordValue recordValue = new RecordValue();
            recordValue.setId(id);
            recordValue.setValue(value);
            recordValue.setUnit(unit);
            recordValue.setTakenAt(takenAt);
            return recordValue;
        }
    }
}
