package com.kkraken.bean.response;

/**
 * Created by Kernel on 11/22/2016.
 */
public class HouseData {
    private Long id;
    private Long userId;
    private String name;
    private Long interval;
    private AddressData address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getInterval() {
        return interval;
    }

    public void setInterval(Long interval) {
        this.interval = interval;
    }

    public AddressData getAddress() {
        return address;
    }

    public void setAddress(AddressData address) {
        this.address = address;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static final class Builder {
        private HouseData entity;

        private Builder() {
            this.entity = new HouseData();
        }

        public static Builder aHouseResponse() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.entity.setId(id);
            return this;
        } public Builder withUserId(Long id) {
            this.entity.setUserId(id);
            return this;
        }

        public Builder withName(String name) {
            this.entity.setName(name);
            return this;
        }

        public Builder withAddress(AddressData address) {
            this.entity.setAddress(address);
            return this;
        }

        public Builder withInterval(Long interval){
            this.entity.setInterval(interval);
            return this;
        }

        public HouseData build() {
            return this.entity;
        }
    }
}
