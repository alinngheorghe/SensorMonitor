package com.kkraken.bean.response;

import java.util.List;

/**
 * Created by Kernel on 11/23/2016.
 */
public class SensorRecordsResponse {
    private Long recordTotal;
    private Long pageTotal;
    private List<RecordValue> values;


    public Long getRecordTotal() {
        return recordTotal;
    }

    public void setRecordTotal(Long recordTotal) {
        this.recordTotal = recordTotal;
    }

    public Long getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Long pageTotal) {
        this.pageTotal = pageTotal;
    }

    public List<RecordValue> getValues() {
        return values;
    }

    public void setValues(List<RecordValue> values) {
        this.values = values;
    }


    public static final class Builder {
        private Long recordTotal;
        private Long pageTotal;
        private List<RecordValue> values;

        private Builder() {
        }

        public static Builder aSensorRecords() {
            return new Builder();
        }

        public Builder withRecordTotal(Long recordTotal) {
            this.recordTotal = recordTotal;
            return this;
        }

        public Builder withPageTotal(Long pageTotal) {
            this.pageTotal = pageTotal;
            return this;
        }

        public Builder withResultLIst(List<RecordValue> values) {
            this.values = values;
            return this;
        }

        public SensorRecordsResponse build() {
            SensorRecordsResponse sensorRecords = new SensorRecordsResponse();
            sensorRecords.setRecordTotal(recordTotal);
            sensorRecords.setPageTotal(pageTotal);
            sensorRecords.setValues(values);
            return sensorRecords;
        }
    }
}

