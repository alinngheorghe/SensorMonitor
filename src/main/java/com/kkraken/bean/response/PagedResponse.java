package com.kkraken.bean.response;

import java.util.List;

/**
 * Created by Alin N. Gheorghe on 16.12.2016.
 */
public class PagedResponse<E> {
    private Integer pageTotal;
    private Long resultTotal;
    private List<E> resultList;

    public Long getResultTotal() {
        return resultTotal;
    }

    public void setResultTotal(Long resultTotal) {
        this.resultTotal = resultTotal;
    }

    public Integer getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Integer pageTotal) {
        this.pageTotal = pageTotal;
    }

    public List<E> getResultList() {
        return resultList;
    }

    public void setResultList(List<E> resultList) {
        this.resultList = resultList;
    }

    public static final class Builder<E> {
        private PagedResponse entity;

        private Builder() {
            this.entity = new PagedResponse();
        }

        public static Builder aPagedResponse() {
            return new Builder();
        }

        public Builder withResultTotal(Long resultTotal) {
            this.entity.setResultTotal(resultTotal);
            return this;
        }

        public Builder withPageTotal(Integer pageTotal) {
            this.entity.setPageTotal(pageTotal);
            return this;
        }

        public Builder withResultList(List<E> resultList){
            this.entity.setResultList(resultList);
            return this;
        }


        public PagedResponse build() {
            return this.entity;
        }
    }
}
