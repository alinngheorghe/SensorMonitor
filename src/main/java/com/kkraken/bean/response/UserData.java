package com.kkraken.bean.response;

import java.security.Principal;

/**
 * Created by Kernel on 11/27/2016.
 */
public class UserData {
    private Long id;
    private String username;
    private String password;
    private String email;
    private String name;

    public UserData() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public static final class Builder {
        private Long id;
        private String username;
        private String password;
        private String email;
        private String name;

        private Builder() {
        }

        public static Builder aUser() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public UserData build() {
            UserData userData = new UserData();
            userData.setId(id);
            userData.setUsername(username);
            userData.setPassword(password);
            userData.setEmail(email);
            userData.setName(name);
            return userData;
        }
    }
}
