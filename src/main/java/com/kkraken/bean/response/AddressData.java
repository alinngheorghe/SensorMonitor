package com.kkraken.bean.response;

/**
 * Created by Kernel on 11/22/2016.
 */
public class AddressData {
    private Long id;
    private Long houseId;
    private String country;
    private String city;
    private String county;
    private String addressLine;
    private Float latitude;
    private Float longitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public static final class Builder {
        private AddressData entity;

        private Builder() {
            this.entity = new AddressData();
        }

        public static Builder anAddress() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.entity.setId(id);
            return this;
        }

        public Builder withHouseId(Long houseId) {
            this.entity.setHouseId(houseId);
            return this;
        }

        public Builder withCountry(String country) {
            this.entity.setCountry(country);
            return this;
        }

        public Builder withCity(String city) {
            this.entity.setCity(city);
            return this;
        }

        public Builder withCounty(String county) {
            this.entity.setCounty(county);
            return this;
        }

        public Builder withAddressLine(String addressLine) {
            this.entity.setAddressLine(addressLine);
            return this;
        }

        public Builder withLatitude(Float latitude) {
            this.entity.setLatitude(latitude);
            return this;
        }

        public Builder withLongitude(Float longitude) {
            this.entity.setLongitude(longitude);
            return this;
        }

        public AddressData build() {
           return this.entity;
        }
    }
}
