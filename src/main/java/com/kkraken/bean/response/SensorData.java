package com.kkraken.bean.response;

import com.kkraken.repository.dao.SensorValue;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

/**
 * Created by Kernel on 11/23/2016.
 */
public class SensorData {
    private Long id;
    private String name;
    private String type;
    private String unit;
    private String status;
    private Long interval;
    private Long houseId;
    private Date createdDate;
    private Date updatedDate;

    private String createdBy;

    private String updatedBy;
    private Date validTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getInterval() {
        return interval;
    }

    public void setInterval(Long interval) {
        this.interval = interval;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public static final class Builder {
        private Long id;
        private String name;
        private String type;
        private String unit;
        private String status;
        private Long interval;
        private Long houseId;
        private Date createdDate;
        private Date updatedDate;
        private String createdBy;
        private String updatedBy;
        private Date validTo;

        private Builder() {
        }

        public static Builder aSensorData() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Builder withUnit(String unit) {
            this.unit = unit;
            return this;
        }

        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder withInterval(Long interval) {
            this.interval = interval;
            return this;
        }

        public Builder withHouseId(Long houseId) {
            this.houseId = houseId;
            return this;
        }

        public Builder withCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Builder withUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
            return this;
        }

        public Builder withCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Builder withUpdatedBy(String updatedBy) {
            this.updatedBy = updatedBy;
            return this;
        }

        public Builder withValidTo(Date validTo) {
            this.validTo = validTo;
            return this;
        }

        public SensorData build() {
            SensorData sensorData = new SensorData();
            sensorData.setId(id);
            sensorData.setName(name);
            sensorData.setType(type);
            sensorData.setUnit(unit);
            sensorData.setStatus(status);
            sensorData.setInterval(interval);
            sensorData.setHouseId(houseId);
            sensorData.setCreatedDate(createdDate);
            sensorData.setUpdatedDate(updatedDate);
            sensorData.setCreatedBy(createdBy);
            sensorData.setUpdatedBy(updatedBy);
            sensorData.setValidTo(validTo);
            return sensorData;
        }
    }
}
