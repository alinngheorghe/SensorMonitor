package com.kkraken.bean.response;

/**
 * Created by Alin N. Gheorghe on 16.12.2016.
 */
public class ChartValue {
    private Long x;
    private Long y;

    public ChartValue(Long x, Long y) {
        this.x = x;
        this.y = y;
    }

    public Long getX() {
        return x;
    }

    public void setX(Long x) {
        this.x = x;
    }

    public Long getY() {
        return y;
    }

    public void setY(Long y) {
        this.y = y;
    }
}
